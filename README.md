# Homeflakes

(one of many) Flaky API Solution(s). This is a solution for the Flaky API Coding Challenge from HomeVision, written in NodeJS.

## The challenge

The coding challenge was pretty straightforward. One must consume an API - provided by HomeVision - and download certain data related to it. The catch of all this is that _the API is (presumably) poorly maintained and therefore it goes down quite often_. One is to handle the API errors and make everything possible to download all the data. One is also to be very careful with this said download, because there's **a lot** of data, and if you don't handle concurrency correctly, it _will_ take an eternity to complete the downloads.

### The approach

I chose to use [Bluebird](https://www.npmjs.com/package/bluebird) to handle concurrency, as I think its concurrency API is more powerful, for this task, than the JS native Promise API. On summary, what the program does is to try to fetch all pages - in parallel - for data. If anything goes wrong and this fetch cannot be completed, it will try to fetch it again to a total of 5 times. If at the 5th attempt the page hasn't been fetched, it will skip it (though, if this actually happened, it's mostly because of an unstable connection).

Once the page was downloaded - and no errors were found - the program will download each image, again in parallel, using as many subprocesses as cpus you have. The 5 attempts are applied here as well; if, for any reason, an image had been tried to be downloaded 5 times, it will be skipped.

Finally, each image is saved in a `homes` dir, at the root of the project (you don't have to create it, as I'm using `fse` and it handles the creation of the dir)

## Tech Stuff

This project was developed using npm 6.14.8 and node v12.19.0.

### Running the project

Once the repo was cloned and `npm i`-ed, run `npm run start`. A `dev` task is available.