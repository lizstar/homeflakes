const Promise = require('bluebird'); // Using blubird for better concurrency management
const api = require('./api')
const { downloadHouseImg, concurrency, log } = require('./utils')

async function fetchAll() {
    for (let i = 1; i <= 10; i++) {
        fetchPage(i)
    }
}

async function fetchPage(n) {
    log.info(`Info: Fetching page ${n}`)
    const response = await api.getHouses(n)
    if (response != null && response.ok) {
        log.success(`Success: Fetched houses from page ${n}. Processing images`)
        return downloadHouses(response.houses, n)
    }

    if (response.err == "ENOTFOUND") {
        log.err(`Error: Cannot reach the API server for page ${n}. Either you do not have a stable internet connection, or API server is down`);
    } else {
        log.warn(`Warning: Could not retrieve houses from page ${n} after 5 tries. Skipping.`);
    }
    return
}

async function downloadHouses(houses, page) {
    try {
        await Promise.map(houses, downloadHouseImg, { concurrency })
    } catch (err) {
        log.err(`Error: There was an error downloading 1 or more images from page ${page} from wich the system could not recover. Skipping the rest`)
    }
    log.success(`Success: Saved all images from page ${page}`)
    return
}

module.exports = {
    start: fetchAll
}