const axios = require("axios")
const retry = require("axios-retry")
const { API, retries } = require("./utils")

retry(axios, { retries }); // Will retry 5 times

module.exports = {
    getHouses: async page => {
        const params = { page }
        try {
            const response = await axios.get(`${API}/houses`, { params })
            return await response.data
        } catch (error) {
            return { err: error.code };
        }
    }
}