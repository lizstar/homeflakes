const chalk = require('chalk')
const download = require('download')
const fse = require('fse')
const path = require('path')
const { paramCase } = require("change-case")
const sanitize = require("sanitize-filename")

const saveDir = path.join(__dirname, '..', 'homes')
const retryAttempts = 5
const concurrency = require('os').cpus().length
const API = "http://app-homevision-staging.herokuapp.com/api_project/"

const log = {
    info: msg => console.log(chalk.blue(msg)),
    warn: msg => console.log(chalk.yellow(msg)),
    success: msg => console.log(chalk.green(msg)),
    err: msg => console.log(chalk.red(msg))
}

async function downloadHouseImg(house, retry = 0) {
    try {
        const file = sanitize(`id-${house.id}-${paramCase(house.address)}.jpg`)
        const fileName = path.join(saveDir, file)
        await fse.writeFile(fileName, await download(house.photoURL))
        log.success(`Success: Downloaded image from  house ${house.id} in ${fileName}`)
    } catch (err) {
        if (retry == retryAttempts) {
            log.err(`Error: Could not download image from house ${house.id} after 5 attempts. Skiping`)
            return null
        }
        log.warn(`Warning: There was an error downloading the house with id ${house.id}. Retrying`)
        return downloadHouseImg(house, retry + 1)
    }
}

module.exports = {
    API,
    concurrency,
    downloadHouseImg,
    log,
    retryAttempts
}